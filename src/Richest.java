import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by jharshman on 2/10/16.
 */
public class Richest {

    public static final int N = 10000;
    public static final int MIN = Integer.MIN_VALUE;
    public static final int START = 1;
    public static void main(String[] args) {

        String fileName;
        File fin;
        Scanner scanner;

        /* get command line param */
        if(args.length < 1)
            System.exit(-1);

        fileName = args[0];

        /* get file handle */
        fin = new File(fileName);
        try {
            scanner = new Scanner(fin);

            while(scanner.hasNextInt()) {
                // read and process N records
                for(int count = 0; count < N; count++) {
                    // add each number to min heap
                }

                // process min heap

                // flush contents out to file on disk

            }

        } catch(FileNotFoundException fnf) {
            fnf.printStackTrace();
        }

    }

    private class MinHeap {

        int[] heap;
        int size;
        int arraySize;

        public MinHeap(int arraySize) {
            this.arraySize = arraySize;
            size = 0;
            heap = new int[arraySize + 1];
            heap[0] = MIN;
        }

        public int getParent(int key) {
            if(Math.floor(key/2) >= 1)
                return (key/2);
            return 0;
        }

        public int getLeftChild(int key) {
            if((2*key) <= size)
                return (2*key);
            return 0;
        }

        public int getRightChild(int key) {
            if((2*key)+1 <= size)
                return (2*key)+1;
            return 0;
        }

        public boolean isLeaf(int key) {
            if(key >= (size/2) && key <= size)
                return true;
            return false;
        }

        public void add(int toAdd) {
            heap[++size] = toAdd;
            int tmp = size;
            while(heap[tmp] < heap[getParent(tmp)]) {
                // swap
                swapPositions(tmp,getParent(tmp));
                // update tmp index
                tmp = getParent(tmp);
            }
        }

        public int delete() {
            int deleted = heap[START];
            heap[START] = heap[size--];
            minHeapify(START);
            return deleted;
        }

        public void buildMaxHeap() {
            for(int key = (int)Math.floor(size/2); key >= 1; key--) {
                minHeapify(key);
            }
        }

        public void minHeapify(int key) {

            if(isLeaf(key) == false) {
                if(heap[key] > heap[getLeftChild(key)] ||
                        heap[key] > heap[getRightChild(key)]) {
                    if(heap[getLeftChild(key)] < heap[getRightChild(key)]) {
                        // swap
                        swapPositions(key, getLeftChild(key));
                        //heapify
                        minHeapify(getLeftChild(key));
                    } else {
                        // swap
                        swapPositions(key, getRightChild(key));
                        // heapify
                        minHeapify(getRightChild(key));
                    }

                }
            }

        }

        public void swapPositions(int a, int b) {
            int c;
            c = heap[a];
            heap[a] = heap[b];
            heap[b] = c;
        }


    }

}
